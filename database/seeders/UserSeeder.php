<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() === 0) {
            $admin = User::firstOrNew(['email' => 'admin@test.com']);
            $admin->password = bcrypt('10script10');
            $admin->first_name = "Admin";
            $admin->last_name = "Test";
            $admin->save();
        }

        if (App::environment('local')) {
            User::factory()->count(6)->create();
            //For showing someone who is invited
            User::factory()->create(['invite_token' => Str::random(10)]);
        }
    }
}
