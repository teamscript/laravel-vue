<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Clientcloud</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    @php
        $manifest = json_decode(file_get_contents(public_path('js/manifest.json')), true);
    @endphp
    @production
        @foreach ($manifest['resources/js/main.js']['imports'] as $importName)
            <link rel="modulepreload" href="/js/{{ $manifest[$importName]['file'] }}" as="script">
        @endforeach
        @foreach ($manifest as $export)
            @if (isset($export['css']))
                @foreach ($export['css'] as $url)
                    <link rel="stylesheet" href="/js/{{ $url }}" />
                @endforeach
            @endif
        @endforeach
    @endproduction
</head>

<body>

    <div id="app"></div>

    <script>
        window.env = {
            NODE_ENV: `{{ config('app.env') }}`,
            BASE_URL: `{{ config('app.url') }}`,
            MIX_PUSHER_APP_KEY: `{{ config('pusher.key') }}`,
            MIX_PUSHER_APP_CLUSTER: `{{ config('pusher.cluster') }}`,
            MIX_APP_URL: `{{ config('app.url') }}`,
            MIX_APP_NAME: `{{ config('app.name') }}`,
        }
        window.process = {
            env: window.env
        };
    </script>
    @production
        <script type="module" src="/js/{{ $manifest['resources/js/main.js']['file'] }}"></script>
    @else
        <script type="module" src="http://localhost:3000/@vite/client"></script>
        <script type="module" src="http://localhost:3000/resources/js/main.ts"></script>
    @endproduction

    {{-- <script>
        // Check if service workers are supported
        if ('serviceWorker' in navigator) {
            window.addEventListener('load', () => {
                navigator.serviceWorker.register('/service-worker.js');
            });
        }
    </script> --}}
</body>

</html>
