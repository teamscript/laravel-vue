// is used when importing from vue-files, so that typescript can declare the type
// https://github.com/vuejs/vue-next-webpack-preview/issues/5
// declare module '*.vue' {
//     import {ComponentOptions} from 'vue';
//     const component: ComponentOptions;
//     export default component;
// }
declare module '*.vue' {
    import type {DefineComponent} from 'vue';
    const component: DefineComponent<unknown, unknown, unknown>;
    export default component;
}
