import {createApp} from 'vue';
import Home from './pages/Home.vue';

const app = createApp(Home);
app.mount('#app');
