<?php

declare(strict_types = 1);

return [

    /*
    |--------------------------------------------------------------------------
    | Authenticatie Taal Regels
    |--------------------------------------------------------------------------
    |
    | De volgende taal regels worden gebruikt gedurende authenticatie voor
    | verschillende berichten die we aan de gebruiker moeten laten zien. Je
    | bent vrij om deze aan te passen, mocht dat nodig zijn voor je applicatie.
    |
     */

    'failed' => 'De ingevoerde gegevens komen niet overeen met onze gegevens.',
    'throttle' => 'Te veel aanmeld pogingen. Probeer het opnieuw na :seconds seconden.',
    'token.missing' => 'Je bent uitgelogd (Code: 1)',
    'token.blacklisted' => 'Je bent uitgelogd (Code: 2)',
    'token.invalid' => 'Je bent uitgelogd (Code: 3)',
    'token.expired' => 'Je bent uitgelogd (Code: 4)',
    'refresh.expired' => 'Je bent uitgelogd (Code: 5)',
];
