<?php

declare(strict_types = 1);

return [

    /*
    |--------------------------------------------------------------------------
    | Validatie Taal Regels
    |--------------------------------------------------------------------------
    |
    | De volgende taal regels bevat de standaard foutmelding die gebruikt wordt
    | door de validatie klasse. Sommige van deze regels hebben meerdere versies,
    | zoals de grootte regels. Je bent vrij om deze berichten aan te passen.
    |
     */

    // Probleem met 'De' en 'Het'. Je zou voor ofwel 'De' ofwel 'Het' specifieke regels moeten schrijven?

    // Toast message:
    'message' => 'Formulier bevat fouten',
    // Validation rules:
    'string' => 'Dit is geen geldige :attribute',
    'required' => 'Vul een :attribute in.',
    'required_with' => 'Het :attribute veld is verplicht wanneer :values aanwezig is.',
    'required_unless' => 'Het :attribute veld is verplicht tenzij :values aanwezig is.',
    'unique' => ':attribute bestaal al',
    'numeric' => 'Het :attribute moet een getal zijn.',
    'digits_between' => ':attribute moet tussen de :min en :max cijfers zijn.',
    'min' => [
        'numeric' => 'De :attribute moet minstens :min zijn.',
        'file' => 'De :attribute moet minstens :min kilobytes zijn.',
        'string' => 'De :attribute moet minstens :min karakters zijn.',
        'array' => 'De :attribute moet op z\'n minst :min items bezitten.',
    ],
    'max' => [
        'numeric' => 'De :attribute mag niet groter zijn dan :max.',
        'file' => 'De :attribute mag niet groter zijn dan :max kilobytes.',
        'string' => 'De :attribute mag niet groter zijn dan :max karakters.',
        'array' => 'De :attribute mag niet meer dan :max items bezitten.',
    ],
    'integer' => ':attribute moet uit nummers bestaan',
    'traject_overlap' => 'Deze periode heeft overlap met een andere traject periode',
    'exists' => 'De geselecteerde :attribute is niet geldig.',
    'email' => 'De :attribute moet een geldig e-mailadres zijn',

    /*
    |--------------------------------------------------------------------------
    | Validatie Taal Regels op maat
    |--------------------------------------------------------------------------
    |
    | Hier kun je validatie berichten op maat specificeren voor attributen,
    | door "attribute.rule" te gebruiken om namen aan regels te geven.
    | Hiermee kun je snel een aangepaste taal regel voor een gegeven
    | attribuut regel specificeren.
    |
     */

    'custom' => [
        'max_day_parts' => [
            'required' => 'Geef het :attribute aan.',
            'min' => 'Geef het :attribute aan.',
        ],
        'message' => [
            'required' => 'Bericht mag niet leeg zijn',
        ],
        'rate' => [
            'regex' => 'Voer een geldig bedrag in',
            'not_in' => 'Voer een geldig bedrag in',
        ],
        'end_date' => [
            'required_unless' => 'Het :attribute veld is verplicht tenzij onbepaalde tijd is aangevinkt.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Validatie Attributen op maat
    |--------------------------------------------------------------------------
    |
    | De volgende taal regels worden gebruikt om de plaatshouder van onze
    | attributen te verwisselen met een wat meer gebruiksvriendelijkere,
    | zoals "E-Mail adres" ipv "email". Dit maakt ons bericht simpelweg
    | expressiever.
    |
     */

    'attributes' => [
        // personal client data
        'first_name' => 'voornaam',
        'last_name' => 'achternaam',
        'street' => 'straatnaam',
        'zipcode' => 'postcode',
        'number' => 'huisnummer',
        'number_addition' => 'toevoeging',
        'city' => 'plaatsnaam',
        'email' => 'E-mailadres',
        'phone_number' => 'telefoonnummer',
        'birth-date' => 'geboortedatum',
        'social_security_number' => 'BSN',
        'income_situation' => 'inkomenssituatie',
        // indication
        'customer_id' => 'klant',
        'rate' => 'tarief',
        'rate_interval' => 'tarief interval',
        'invoice_interval' => 'factuur interval',
        'start_date' => 'startdatum',
        'end_date' => 'einddatum',
        'max_day_parts' => 'maximum aantal dagdelen',
        'indication_type' => 'indicatie type',
        'allocation_number' => 'toewijzingsnummer',
        'product_code' => 'productcode',
        // casemanagement
        'status_id' => 'status',
        'ict_mentor_id' => 'ict begeleider',
        'care_mentor_id' => 'zorg begeleider',
        'start_date' => 'startdatum',
        'end_date' => 'einddatum',
        'last_evaluation' => 'laatste evaluatie datum',
        'last_mdo' => 'laatste besproken MDO datum',
        // general
        'name' => 'naam',
        // reden van afmelding
        'reason' => 'reden',
        // indications
        'customer_id' => 'klant',
        'rate' => 'tarief',
        'rate_interval' => 'tarief interval',
        'invoice_interval' => 'factuur interval',
        'start_date' => 'startdatum',
        'end_date' => 'einddatum',
        'max_day_parts' => 'maximum aantal dagdelen',
        'invoice_email' => 'factuur e-mail',
        // learning goals
        'title' => 'titel',
        'learning_goal' => 'leerdoel',
        'reasoning' => 'onderbouwing',
        'progress' => 'voortgang',
        // auth
        'password' => 'wachtwoord',
        // mdo
        'clients.*.notes' => 'notulen',
        'clients.*.policies' => 'beleid',
        'policies' => 'beleid',
        'notes' => 'notulen',
        // mentors
        'ambulatory' => 'ambulant',
        //contacts
        'new_organisation.name' => 'organisatie',
        // contact reports
        'contacts' => 'contact',
        'report' => 'rapport',
        // production accountability
        'year' => 'jaar',
        'financing_type_id' => 'zorgwet',
        // absence protocol
        'absence_protocol' => 'afwezigheidsprotocol',
    ],
];
