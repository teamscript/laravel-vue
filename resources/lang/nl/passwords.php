<?php

declare(strict_types = 1);

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | De volgende taal regels zijn de standaard regels om redenen te koppelen
    | die gegeven zijn door de wachtwoord tussenpersoon (password broker) voor
    | het falen van het bijwerken van een wachtwoord, zoals een ongeldige
    | token of een ongeldig nieuw wachtwoord.
    |
    */

    'reset' => 'Je password is gereset!',
    'sent' => 'We hebben je een wachtwoord reset link gestuurd!',
    'throttled' => 'Wacht AUB voor je opnieuw probeert.',
    'token' => 'Deze wachtwoord reset token is ongeldig.',
    'user' => "We kunnen geen gebruiker vinden met dat email adres.",

];
