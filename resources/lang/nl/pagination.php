<?php

declare(strict_types = 1);

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Taal Regels
    |--------------------------------------------------------------------------
    |
    | De volgende taal regels worden gebruikt door de paginator bibliotheek om
    | de simpele pagination links te bouwen. Je bent vrij om deze aan te
    | passen, mocht dat nodig zijn voor je applicatie.
    |
    */

    'previous' => '&laquo; Vorige',
    'next' => 'Volgende &raquo;',

];
