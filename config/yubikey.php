<?php

declare(strict_types = 1);

/*
 * You will need an API Key from the official Yubikey Website
 * https://upgrade.yubico.com/getapikey/
 * */

return [
    'CLIENT_ID' => '63533',
    'SECRET_KEY' => 'hPAD4SGWg3GIr0QtvxSKRBvmuaw=',
    'URL_LIST' => [
        'api.yubico.com/wsapi/2.0/verify',
        'api2.yubico.com/wsapi/2.0/verify',
        'api3.yubico.com/wsapi/2.0/verify',
        'api4.yubico.com/wsapi/2.0/verify',
        'api5.yubico.com/wsapi/2.0/verify',
    ],
    'USER_AGENT' => 'Laravel 8',
    'HTTPS_VERIFY' => true,
    'CAINFO_LOCATION' => '',
];
