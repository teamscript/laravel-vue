# How to start This project in production mode
To start the program in production mode a few stepps need to be taken.

### compile javascript and clear old file cache
Run the following in the terminal
```sh
php artisan optimize:clear #clears all cashed settings and configurations
npm run build  #compiles the javescript down to its final form.
```
You need to rerun they above  steps wheenver you make change to the javaescript or they php config files.
ALso if your env file changes.

Starting the server this starts the server
```sh
 APP_ENV=production php artisan --env=production serve --port 8080
```
Makes sure you have a env file called `.env.production`
Optional step is to just make a copy of  `.env` and call it `.env.production`
Make sure the `env.production` has its APP_URl set to the correct port.
also you can create a separate Database and set its settings in that file.
```sh
#.env.production
APP_URL=http://localhost:8080
DB_DATABASE=production #this assumes you created a new database entry called `production` you can use any name you want
```
After every change to they env files you must shutdown any running php process and then run ` php artisan optimize:clear`
A configuration cache exists that will store they old values on disk.

###Migration they database.
When running any artisan commands like for example the artisan migrate command.
use the `--env=production` to run it with the correct settings.
With production enabled not all seeders will run even if you use they `--seed` flag.
You can use the normal `php artisan migrate:fresh --seed`  command to seed the database.
With development values just change in your the `.env` file the following `DB_DATABASE=production`


