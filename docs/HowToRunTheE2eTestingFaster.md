### Quick tips to cypress
Cookies get deleted after each and every test.
So make sure you run the cy.login() before each test.
exmaple good
```js
        beforeEach(() => {
            cy.login();
            cy.visit('/example-page');
        });
        it('test1',()=>{
            ...
        })
        it('test1'()=>{
            ...
        })
```
exmaple bad
```js
        before(() => {
            cy.login();
            cy.visit('/example-page');
        }); // Any retry o a failed will skip this login
        ...
```

If you want to run only the test you are working you can use `.only(`
```js
        it.only('only this test will run'()=>{
            ...
        })
```
You can also use `.skip` to skip a test.

## Run the test against the poduction build.

Cypress runs faster if you start up they program with they production settings on.
[See this file how to do it](../docs/StartProductionServer.md)

Note
`Cypress.json` contains retry values. which will retry failed a number of times depending on its value.
Also `MAIL_MAILER=log` is a usefull settings to be able to ignore emails being send


###
When running your project on a diffrent url use the -c to change a setting
```sh
npx cypress open -c baseUrl=http://localhost:8080
```
When you wnat to change more than 1 setting use
```
 npx cypress run -c baseUrl=http://localhost:8080,video=false
```
