# Rapp

## What the app does

This app is a

### Project setup

```sh
npm install
npm run build

composer install

mysql -u root
# run: create database;

cp .env.example .env
code .env
# edit if needed: DB_PORT=, DB_DATABASE=, DB_USERNAME=, DB_PASSWORD=

php artisan key:generate
php artisan migrate:fresh --seed
```

### Run project

Run in 1 terminal the javascript development server:

```sh
npm run watch
```

Run in another terminal the laravel development server:

```sh
php artisan serve
```

Login data can be found in: UserSeeder.php _(vscode: `ctrl` + `p` then type `UserSeeder.php`)_

### Developing project

When making Models, extend with Eloquent to get autocomplete in vscode

After creating or updating a Model, run the following to update autocompletion in vscode:

```sh
php artisan ide-helper:models
```
