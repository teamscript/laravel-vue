import {InlineConfig} from 'vitest';
import {NodeTransform} from '@vue/compiler-core';
import {PluginOption, defineConfig} from 'vite';
import {visualizer} from 'rollup-plugin-visualizer';
import copy from 'rollup-plugin-copy';
import path from 'path';
import vue, {Options} from '@vitejs/plugin-vue';

const srcPath = path.resolve('./resources/js');

const resolve = {
    alias: {
        // general aliases
        components: path.join(srcPath, 'components'),
        errors: path.join(srcPath, 'errors'),
        helpers: path.join(srcPath, 'helpers'),
        layouts: path.join(srcPath, 'layouts'),
        pages: path.join(srcPath, 'pages'),
        types: path.join(srcPath, 'types'),
    },
};

const testAttributes = ['data-test-id', 'data-test-class'];

const testNodeTransformer: NodeTransform = node => {
    if (node.type !== 1 /** NodeTypes.ELEMENT  */) return;
    for (let index = 0; index < node.props.length; index++) {
        const {type, name} = node.props[index];
        // eslint-disable-next-line no-magic-numbers
        if (type === 6 /** NodeTypes.ATTRIBUTE */ && testAttributes.includes(name)) {
            node.props.splice(index, 1);
            index--;
        }
    }
};

const componentTestConfig: InlineConfig = {
    environment: 'jsdom',
    include: ['tests/js/component/**/*.spec.ts'],
    coverage: {
        reporter: ['lcov', 'json', 'json-summary'],
    },
    setupFiles: 'tests/js/component/setup.ts',
};

const getPlugins = (production: boolean): (PluginOption | PluginOption[])[] => {
    const vueOptions: Options = {};

    if (production) vueOptions.template = {compilerOptions: {nodeTransforms: [testNodeTransformer]}};

    const plugins = [
        copy({
            targets: [{src: 'resources/img', dest: 'public/images'}],
            hook: 'writeBundle',
        }),
        vue(vueOptions),
    ];

    // Generates stats.html file with the bundle stats handy for reducing the main bundle size
    if (production) plugins.push(visualizer({}));
    return plugins;
};

export default defineConfig(({command}) => {
    const production = command !== 'serve';

    return {
        base: production ? '/js/' : '',
        build: {
            target: 'es2022',
            assetsInclude: [],
            manifest: true,
            outDir: 'public/js',
            rollupOptions: {
                input: 'resources/js/main.ts',
            },
        },
        test: componentTestConfig,
        plugins: getPlugins(production),
        server: {port: 3000},

        // Vite bugs if you build out dir is inside the public dir so we change that to something else
        publicDir: 'random_non_existent_folder',
        resolve,
    };
});
