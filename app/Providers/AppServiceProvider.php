<?php

declare(strict_types = 1);

namespace App\Providers;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Telescope\TelescopeServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * This code fixes a bug similar to this:
         * https://github.com/laravel/telescope/issues/347
         *
         * This is checked via strpos - strpos returns an integer if PHPUnit is found,
         * and false if it is not
         */

        if ($this->app->environment('local')) {
            $this->app->register(TelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        JsonResource::withoutWrapping();
        //This should be fix for max key length error in deployment
        Schema::defaultStringLength(191);
    }
}
