<?php

declare(strict_types = 1);

namespace App\Models;

use Eloquent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;

/**
 * User
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string $email
 * @property string $password
 * @property string|null $invite_token
 * @property string|null $password_reset_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method   static \Database\Factories\UserFactory factory(...$parameters)
 * @method   static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method   static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method   static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method   static \Illuminate\Database\Eloquent\Builder|User query()
 * @method   static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method   static \Illuminate\Database\Eloquent\Builder|User whereDeletedAt($value)
 * @method   static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method   static \Illuminate\Database\Eloquent\Builder|User whereFirstName($value)
 * @method   static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method   static \Illuminate\Database\Eloquent\Builder|User whereInviteToken($value)
 * @method   static \Illuminate\Database\Eloquent\Builder|User whereLastName($value)
 * @method   static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method   static \Illuminate\Database\Eloquent\Builder|User wherePasswordResetToken($value)
 * @method   static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method   static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method   static \Illuminate\Database\Query\Builder|User withoutTrashed()
 */
class User extends Eloquent implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, SoftDeletes, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
}
