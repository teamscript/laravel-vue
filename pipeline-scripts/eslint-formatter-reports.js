/* eslint-disable no-console */
/* eslint-env node */
/**
 * Based on the reporter from https://github.com/spartez/eslint-formatter-bitbucket-reports
 */
import path from 'path';
import fs from 'fs';
import {bitbucketShortCommit, processResults} from './reporter.js';

const SEVERITIES = {
    1: 'MEDIUM',
    2: 'HIGH',
};

const generateReport = results => {
    const {errorCount, warningCount} = results.reduce(
        (acc, current) => {
            acc.errorCount += current.errorCount;
            acc.warningCount += current.warningCount;
            return acc;
        },
        {errorCount: 0, warningCount: 0},
    );

    const problemCount = errorCount + warningCount;

    const details = `${problemCount} problem${problemCount !== 1 ? 's' : ''} (${errorCount} error${
        errorCount !== 1 ? 's' : ''
    }, ${warningCount} warning${warningCount !== 1 ? 's' : ''})`;

    const result = errorCount > 0 ? 'FAILED' : 'PASSED';

    return {
        title: 'ESLint report',
        reporter: 'ESLint',
        report_type: 'COVERAGE',
        details,
        result,
    };
};

const generateAnnotations = (reportId, results) => {
    return results.reduce((acc, result) => {
        const relativePath = path.relative(process.cwd(), result.filePath);

        return [
            ...acc,
            ...result.messages.map((messageObject, i) => {
                const {line, message, severity, ruleId} = messageObject;
                const external_id = `${reportId}-${relativePath}-${line}-${ruleId}-${i}`;
                return {
                    external_id,
                    line,
                    path: relativePath,
                    summary: `${message} (${ruleId})`,
                    annotation_type: 'BUG',
                    severity: SEVERITIES[severity],
                };
            }),
        ];
    }, []);
};

(async () => {
    const rawEslintOutput = fs.readFileSync(`${process.cwd()}/eslint.out`);
    const results = JSON.parse(rawEslintOutput);
    const reportId = `Eslint-${bitbucketShortCommit}`;

    await processResults(reportId, generateReport(results), generateAnnotations(reportId, results));
})();
