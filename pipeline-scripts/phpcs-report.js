/* eslint-disable no-console */
/* eslint-env node */
/**
 * Based on the reporter from https://github.com/spartez/eslint-formatter-bitbucket-reports
 */
import path from 'path';
import fs from 'fs';
import {bitbucketShortCommit, processResults} from './reporter.js';

const SEVERITIES = {
    1: 'MEDIUM',
    2: 'HIGH',
};

const generateReport = summary => {
    const {errors, warnings} = summary;
    const problemCount = errors + warnings;

    const details = `${problemCount} problem${problemCount !== 1 ? 's' : ''} (${errors} error${
        errors !== 1 ? 's' : ''
    }, ${warnings} warning${warnings !== 1 ? 's' : ''})`;

    const result = errors > 0 ? 'FAILED' : 'PASSED';

    return {
        title: 'PHPLint report',
        reporter: 'phpcs',
        report_type: 'TEST',
        details,
        result,
    };
};

const generateAnnotations = (results, reportId) => {
    return Object.keys(results).reduce((acc, filePath) => {
        const relativePath = path.relative(process.cwd(), filePath);

        return [
            ...acc,
            ...results[filePath].messages.map((messageObject, i) => {
                const {line, message, severity, source} = messageObject;
                const external_id = `${reportId}-${relativePath}-${line}-${source}-${i}`;
                return {
                    external_id,
                    line,
                    path: relativePath,
                    summary: `${message} (${source})`,
                    annotation_type: 'BUG',
                    severity: SEVERITIES[severity],
                };
            }),
        ];
    }, []);
};

(async () => {
    const rawOutput = fs.readFileSync(`${process.cwd()}/phpcs.out`);
    const results = JSON.parse(rawOutput);
    const reportId = `phpcs-${bitbucketShortCommit}`;

    await processResults(reportId, generateReport(results), generateAnnotations(reportId, results));
})();
