/* eslint-disable no-console */
/* eslint-env node */
/**
 * Based on the reporter from https://github.com/spartez/eslint-formatter-bitbucket-reports
 */
import got from 'got';

const {BITBUCKET_WORKSPACE, BITBUCKET_REPO_SLUG, BITBUCKET_COMMIT} = process.env;

export const bitbucketShortCommit = BITBUCKET_COMMIT.slice(0, 12);

const BITBUCKET_API_HOST = 'api.bitbucket.org';

const MAX_ANNOTATIONS_PER_REQUEST = 100;

const httpClient = got.extend({
    prefixUrl: 'http://localhost:29418',
    responseType: 'json',
    headers: {
        Host: BITBUCKET_API_HOST,
    },
    hooks: {
        beforeRequest: [
            options => {
                options.path = `http://${BITBUCKET_API_HOST}${options.url.pathname}`;
            },
        ],
    },
});

const generateUrl = reportId =>
    `2.0/repositories/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/commit/${BITBUCKET_COMMIT}/reports/${reportId}`;

const deleteReport = async reportId => httpClient.delete(generateUrl(reportId));

const createReport = async (reportId, report) =>
    httpClient.put(generateUrl(reportId), {
        json: report,
        responseType: 'json',
    });

const createAnnotations = async (reportId, annotations) => {
    const response = await httpClient.post(`${generateUrl(reportId)}/annotations`, {
        json: annotations.slice(0, MAX_ANNOTATIONS_PER_REQUEST),
        responseType: 'json',
    });
    if (annotations.length > MAX_ANNOTATIONS_PER_REQUEST) {
        return createAnnotations(annotations.slice(MAX_ANNOTATIONS_PER_REQUEST));
    }
    return response;
};

export const processResults = async (reportId, report, annotations) => {
    try {
        await deleteReport(reportId);
        console.log('Previous report deleted');
        await createReport(reportId, report);
        console.log('New report created');
        if (annotations.length) {
            await createAnnotations(reportId, annotations);
            console.log('Annotations added');
        }
    } catch (error) {
        if (error.request) {
            console.log(error.request.options);
        }
        if (error.response) {
            console.error(error.message, error.response.body);
        } else {
            console.error(error);
        }
    }
};
